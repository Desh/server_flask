import os, sys, shlex, time
from subprocess import Popen, PIPE
from flask import Flask, request, jsonify, make_response, abort
from flask import url_for, redirect, render_template
from werkzeug.utils import secure_filename
from celery import Celery
from config import *

app = Flask(__name__)


ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

app.config['UPLOAD_FOLDER_IMG'] = UPLOAD_FOLDER_IMG
app.config['UPLOAD_FOLDER_STYLE'] = UPLOAD_FOLDER_STYLE
app.config['CELERY_BROKER_URL'] = REDIS_HOST_PORT
app.config['CELERY_RESULT_BACKEND'] = REDIS_HOST_PORT
app.config['CELERY_MAX_TASKS_PER_CHILD'] = 1


celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


images = [
    {
        'id': 1,
        'image': 'images/img_2.jpg',
        'style': 'styles/style_2.jpg',
        'status': 'SUCCESS',
        'iterations': 15,
        'output': 'res_1'
    }
]

count_ready_img = 1


@app.route('/', methods=['GET'])
def show_landing_page():
    return render_template('index.html')


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def set_filename(filename):
    expansion = secure_filename(filename).rsplit('.', 1)[1]
    return str(images[-1]['id'] + 1) + '.' + expansion


@app.route('/api/v1.0/tasks/revoke/<task_id>', methods=['DELETE'])
def revoke_task(task_id):
    celery.control.revoke(task_id=task_id)
    return redirect(LOCALHOST, code=202)


@app.route('/api/v1.0/image/new/', methods=['POST'])
def get_new_image():
    print(request.files['file_style'])
    print(request.form['iter'])
    iterations = request.form['iter']
    file_img = request.files['file_img']
    file_style = request.files['file_style']
    if (file_img and allowed_file(file_img.filename)) \
            and (file_style and allowed_file(file_style.filename)):

        filename_img = 'img_' + set_filename(file_img.filename)
        print(os.path.join(app.config['UPLOAD_FOLDER_IMG'],
              filename_img))

        file_img.save(os.path.join(app.config['UPLOAD_FOLDER_IMG'],
                                   filename_img))
        filename_style = 'style_' + set_filename(file_img.filename)
        print(file_style)
        file_style.save(os.path.join(app.config['UPLOAD_FOLDER_STYLE'],
                                     filename_style))
        count_ready_img = images[-1]['id'] + 1
        out_name = 'out_{}'.format(count_ready_img)
        images.append(
            {
                'id': images[-1]['id'] + 1,
                'image': filename_img,
                'style': filename_style,
                'iterations': iterations,
                'status': 'PREPARE',
                'output': out_name
            }
        )
        task = long_task.apply_async(args=[list(images[-1].items())])
        time.sleep(1)
        return redirect(url_for('task_status', task_id=task.id, image_id=images[-1]['id'] - 1), code=302)
    return jsonify(state='bad request', code=400)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/status/<task_id>')
def task_status(task_id):
    task = long_task.AsyncResult(task_id)
    image_id = int(request.args['image_id'])
    print(image_id)
    if task.info is not None:
        response = {
            'state': task.state,
            'iteration': task.info.get('iteration', 0),
            'hole':  task.info.get('hole', 0),
            'task_id': task_id,
            'output': images[image_id].get('output', '-')
        }
    else:
        response = {
            'state': task.state,
            'progress': 'in queue',
            'task_id': task_id,
            'output': images[image_id].get('output', '-')
        }
    return jsonify(response)


@celery.task(bind=True)
def long_task(self, values):
    print(values)
    for i in range(len(values)):
        if values[i][0] == 'image':
            img_name = values[i][1]
        elif values[i][0] == 'style':
            style_name = values[i][1]
        elif values[i][0] == 'iterations':
            iterations = str(values[i][1])
    self.update_state(state='BEGIN', meta={'iteration': 0, 'hole': iterations})

    proc = Popen(shlex.split('python neural_artistic_style.py --iterations {}'
                             ' --subject {} --style {}'.format(iterations, img_name, style_name)), stdout=PIPE, stderr=PIPE)
    print('begin')
    for line in proc.stdout:
        out = line.decode().strip()
        print(out, end='')
        self.update_state(state='PROGRESS', meta={'iteration': out, 'hole': iterations})
    proc.wait()
    return {'state': 'Task completed!', 'hole': iterations, 'iteration': iterations}


if __name__ == '__main__':
    app.debug = True
    app.run()
