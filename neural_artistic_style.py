import time
from argparse import ArgumentParser
import sys
def build_parser():
    parser = ArgumentParser()
    parser.add_argument('--iterations', required=True, type=int)
    parser.add_argument('--subject', required=True, type=str,
                        help='Subject image.')
    parser.add_argument('--output', default='out.jpg')
    parser.add_argument('--style', required=True, type=str,
                        help='Style image.')
    return parser

def main():
    parser = build_parser()
    args = parser.parse_args()
    print("subject img: {}".format(args.subject))
    print("style ing: {}".format(args.style))
    if args.output is not None:
        print("out img {}".format(args.output))
    for i in range(args.iterations):
        print(i)
        time.sleep(1)


if __name__ == '__main__':
    main()
